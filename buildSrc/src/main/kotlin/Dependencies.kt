object Dependencies {

    /**
     * lombok
     */
    const val lombok = "org.projectlombok:lombok:${Versions.lombok}"

    /**
     * spring boot starter test
     */
    const val springBootStarterTest = "org.springframework.boot:spring-boot-starter-test"

    /**
     * junit jupiter engine
     */
    const val junitJupiterEngine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junitJupiterEngine}"

    /**
     * spring boot auto configure processor
     */
    const val springBootAutoConfigureProcessor = "org.springframework.boot:spring-boot-autoconfigure-processor"

    /**
     * spring boot starter
     */
    const val springBootStarter = "org.springframework.boot:spring-boot-starter"
}
