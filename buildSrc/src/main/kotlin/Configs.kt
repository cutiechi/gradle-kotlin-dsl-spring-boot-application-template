import org.gradle.api.JavaVersion.VERSION_1_8

object Configs {

    /**
     * root project name
     */
    const val rootProjectName = "spring-boot-application-template"

    /**
     * group
     */
    const val group = "com.cutiechi.template"

    /**
     * version
     */
    const val version = "0.0.1"

    /**
     * java version
     */
    val javaVersion = VERSION_1_8
}
