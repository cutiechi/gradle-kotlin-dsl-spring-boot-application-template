object Versions {

    /**
     * spring boot
     */
    const val springBoot = "2.1.4.RELEASE"

    /**
     * lombok
     */
    const val lombok = "1.18.6"

    /**
     * junit jupiter engine
     */
    const val junitJupiterEngine = "5.3.2"
}
