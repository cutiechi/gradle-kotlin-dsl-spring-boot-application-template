object Plugins {

    /**
     * spring boot
     */
    const val springBoot = "org.springframework.boot"

    /**
     * spring dependency management
     */
    const val springDependencyManagement = "io.spring.dependency-management"
}
