plugins {
    java
    id(Plugins.springBoot).version(Versions.springBoot)
}

apply(plugin = Plugins.springDependencyManagement)

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = Configs.javaVersion
    targetCompatibility = Configs.javaVersion
}

group = Configs.group

version = Configs.version

dependencies {

    implementation(Dependencies.springBootStarter)

    annotationProcessor(Dependencies.lombok)
    annotationProcessor(Dependencies.springBootAutoConfigureProcessor)

    testImplementation(Dependencies.junitJupiterEngine)
    testImplementation(Dependencies.springBootStarterTest) {
        exclude("junit", "junit")
    }

    testAnnotationProcessor(Dependencies.lombok)
}
