package com.cutiechi.template;

import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Spring Boot template application base test class
 *
 * @author Cutie Chi
 * @version 0.0.1
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpringBootTemplateApplicationTest {

}
