package com.cutiechi.template;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

/**
 * Spring Boot application template bootstrap class
 *
 * @author Cutie Chi
 * @version 0.0.1
 */
@SpringBootApplication
public class SpringBootTemplateApplication {

    public static void main (final String[] arguments) {
        run(SpringBootTemplateApplication.class, arguments);
    }
}
